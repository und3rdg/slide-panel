$(function() {
    $('ul#btn li').on('click', slidePanel);

    function slidePanel() {
        $('ul#btn li.active').removeClass('active');
        $(this).addClass('active');

        var $panelId = $(this).attr('data-panelid');

        $('.panel.active').slideUp(300, function() {
            $('.'+$panelId).slideDown(300, function() {
                $('.panel.active').removeClass('active');
                $('.'+$panelId).addClass('active');  
            });
        });

    }


    // find highest panel
    var $pan = $('.panel');
    var $panCount = $pan.length;
    var $maxPanHeight = 0;

    for(var i = 0; i < $panCount; i++) {
        var $panHeight = $pan.eq(i).outerHeight();
        if($maxPanHeight < $panHeight) {
            $maxPanHeight = $panHeight;
        };
    };

    var $btnHeight = $('ul#btn li').outerHeight();
    var $containerMaxHeight = $btnHeight + $maxPanHeight;
    $('div#container').css({
        minHeight: $containerMaxHeight
    });
});


